export default function ({
    $axios, redirect, app, store,
}) {
    $axios.onError((error) => {
        const { errors } = error.response.data;

        return Promise.resolve(errors);
    });
}