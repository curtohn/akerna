export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'arkerna-client',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },

    components: true,

    css: [
        // 
    ],

    plugins: [
        // 
        '~/plugins/bootstrap.js'
    ],

    modules: [
        // Client/server-side HTTP library
        '@nuxtjs/axios', 

        // CSS Framework
        'bootstrap-vue/nuxt',
    ],

    buildModules: [
        // 
    ],

    build: {
        // 
    },

    router: {
        middleware: [
            // 
        ]
    },

    publicRuntimeConfig: {
        apiURL: process.env.API_URL || 'http://localhost:82/v1',
    },

    privateRuntimeConfig: {
        apiURL: process.env.API_URL || 'http://localhost:82/v1',
    }
}
