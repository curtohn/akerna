export const state = () => ({
    user: {
        name: null, 
        caffeineConsumption: 0,
    },
})

export const mutations = {
    resetCaffeine: (state) => {
        state.user.caffeineConsumption = 0
    },

    addCaffeine: (state, caffeinatedDrink) => {
        state.user.caffeineConsumption += caffeinatedDrink.caffeine
    },

    setUserName(state, name) {
        state.user.name = name
    },

    setUserCaffeineConsumption(state, name) {
        state.user.caffeineConsumption = name
    }
}