<?php

//////////////////////////////
// Akerna - PHP Assessment
//////////////////////////////

// Load in third-party assets managed by Composer
require_once './vendor/autoload.php';

$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

// Database intialization
require_once './configuration/database.php';

// Cache intialization
require_once './configuration/cache.php';

// Validation intialization
require_once './configuration/validation.php';

// API initialization
require_once './configuration/api.php';

// TODO: Implement the following:
// - Add instrumentation to the API

// Finally, run our application
$application->run();