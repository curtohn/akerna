<?php

return
[
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds'
    ],

    'environments' => [
        'default_environment' => 'default',
        'default_migration_table' => 'phinx_migrations',
        
        'default' => [
            'adapter' => 'mysql',
            
            'host' => env('DATABASE_HOST'),
            'name' => env('DATABASE_NAME'),
            'port' => env('DATABASE_PORT'),

            'user' => env('DATABASE_USERNAME'),
            'pass' => env('DATABASE_PASSWORD'),
            
            'charset' => 'utf8',
        ],

    ],

    'version_order' => 'creation'
];
