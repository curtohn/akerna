<?php 

namespace Akerna\Utilities;

class APIResponse 
{
    public static $request = null;
    public static $response = null;

    public static function success($data = [], $status = 200) 
    {
        $response = self::$response;

        $response
            ->getBody()
            ->write(
                json_encode($data)
            );

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($status);
    }

    public static function error($data = [], $status = 500)
    {
        $response = self::$response;

        $response
            ->getBody()
            ->write(
                json_encode($data)
            );

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($status);
    }
}