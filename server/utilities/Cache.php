<?php 

namespace Akerna\Utilities;

class Cache 
{
    static $redisClient;
    
    public static function get(string $key)
    {
        $data = self::$redisClient->get($key);

        if (!$data) {
            return false;
        }

        try {
            return json_decode($data);
        } catch (\Exception $e) {
            return $data;
        }

        return false;
    }

    public static function set(string $key, $data, int $expiration = 86400) 
    {
        if (!is_string($data)) {
            $data = json_encode($data);
        }

        self::$redisClient->set($key, $data, 'EX', $expiration);
    }

    public static function del(string $key) 
    {
        self::$redisClient->del($key);
    }
}