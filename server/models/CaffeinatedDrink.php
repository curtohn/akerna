<?php 

namespace Akerna\Models;

use Illuminate\Database\Eloquent\Model;

class CaffeinatedDrink extends Model
{
    // 
    public $timestamps = true;
    
    //
    protected $table = 'caffeinated_drinks';
    protected $guarded = [];
}
