<?php 

//////////////////////////////
// Cache Configuration
//////////////////////////////

// Parameters passed using a named array:
$redisClient = new \Predis\Client([
    'scheme' => env('REDIS_SCHEMA'),
    'host'   => env('REDIS_HOST'),
    'port'   => env('REDIS_PORT'),
]);

// Set the instance onto the cache utility class
\Akerna\Utilities\Cache::$redisClient = $redisClient;