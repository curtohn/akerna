<?php 

//////////////////////////////
// Validation Configuration
//////////////////////////////

// 
function initializeValidation() 
{
    $filesystem = new \Illuminate\Filesystem\Filesystem();
    $fileLoader = new \Illuminate\Translation\FileLoader($filesystem, '');
    $translator = new \Illuminate\Translation\Translator($fileLoader, 'en_US');
    
    // 
    $factory = new \Illuminate\Validation\Factory($translator);

    // Used for database (uniqueness) checks
    $verifier = new \Illuminate\Validation\DatabasePresenceVerifier(
        $GLOBALS['capsule']->getDatabaseManager()
    );

    $factory->setPresenceVerifier($verifier);

    // 
    return $factory;
}

$GLOBALS['validationFactory'] = initializeValidation();
