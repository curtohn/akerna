<?php

//////////////////////////////
// API Configuration
//////////////////////////////

// Utility imports
use Akerna\Utilities\APIResponse;

// Third-party module imports
use Slim\Factory\AppFactory;
use Slim\Routing\RouteCollectorProxy;

use Slim\Psr7\Response;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

// Create an instance of a Slim application; this is the main entry point for the application,
// and will handle all requests via HTTP(S)
$application = AppFactory::create();

$helperMiddleware = function (Request $request, RequestHandler $handler) use ($application) {
    $response = $application->getResponseFactory()->createResponse();

    APIResponse::$request = $request;
    APIResponse::$response = $response;

    return $handler->handle($request);
};

$application->add($helperMiddleware);

// CORS configuration
$corsMiddleware = function (Request $request, RequestHandler $handler) {
    $response = $handler->handle($request);
  
    // NOTE: This * would be changed on Production (for the purposes of development, it was left to allow connections from 
    // any origin).
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
};

$application->add($corsMiddleware);

// JSON parsing middleware
$application->addBodyParsingMiddleware();

$application->group('/v1', function (RouteCollectorProxy $group) {
    // "glob" is a PHP function that will find all files in a directory, and return them as an array
    $apiFiles = glob('api/*.route.php');

    foreach ($apiFiles as $apiFile) {
        // We're using "include" over "require" here because we want to continue execution if 
        // a specific route fails, with the assumption that our APM and synthetic checks will 
        // determine/alert us
        include $apiFile;
    }
});

// 
$application->options('/{path:.+}', function ($request, $response, $args) {
    return $response;
});

// Fallback route to handle the serving of our templates
$application->get('/{path:.*}', function ($request, $response, array $args) {
    $response
        ->getBody()
        ->write('Hello');

    return $response;
});