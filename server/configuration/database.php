<?php 

//////////////////////////////
// Database Configuration
//////////////////////////////

// 
$capsule = new Illuminate\Database\Capsule\Manager;

$capsule->addConnection([
    'driver' => 'mysql',
    
    'host' => env('DATABASE_HOST'),
    'database' => env('DATABASE_NAME'),
    'username' => env('DATABASE_USERNAME'),
    'password' => env('DATABASE_PASSWORD'),
    
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
]);

$capsule->setEventDispatcher(
    new Illuminate\Events\Dispatcher(
        new Illuminate\Container\Container
    )
);

// 
$capsule->setAsGlobal();

// 
$capsule->bootEloquent();
