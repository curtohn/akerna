<?php 

// Third-party module imports
use Slim\Routing\RouteCollectorProxy;

// Health check for Kubernetes (K8S)
$group->group('/caffeinated-drinks', function (RouteCollectorProxy $coffeeGroup) {
    // Get All|Get One operations
    
    /**
     * @api {GET} /caffeinated-drinks Get all Caffeinated Drinks
     */
    $coffeeGroup->get('', Akerna\Controllers\CaffeinatedDrinkController::class . ':all');

    /**
     * @api {GET} /caffeinated-drinks/{id} Get a Caffeinated Drink
     */
    $coffeeGroup->get('/{id}', Akerna\Controllers\CaffeinatedDrinkController::class . ':get');

    // Create|Update|Delete operations

    /**
     * @api {POST} /caffeinated-drinks Create a Caffeinated Drink
     */
    $coffeeGroup->post('', Akerna\Controllers\CaffeinatedDrinkController::class . ':create');

    /**
     * @api {PUT} /caffeinated-drinks/{id} Update a Caffeinated Drink
     */
    $coffeeGroup->put('/{id}', Akerna\Controllers\CaffeinatedDrinkController::class . ':update');
    
    /**
     * @api {DELETE} /caffeinated-drinks/{id} Deelte a Caffeinated Drink
     */
    $coffeeGroup->delete('/{id}', Akerna\Controllers\CaffeinatedDrinkController::class . ':delete');
});