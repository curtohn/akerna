<?php 

// Third-party module imports
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

// Health check for Kubernetes (K8S) to avoid inadvertent termination of the pod/container
$group->get('/healthz', function (Request $request, Response $response, array $args) {
    $response
        ->getBody()
        ->write('On');
    
    return $response;
});