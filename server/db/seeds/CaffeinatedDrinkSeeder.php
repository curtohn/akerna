<?php


use Phinx\Seed\AbstractSeed;

class CaffeinatedDrinkSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Monster Ultra Sunrise',
                'caffeine' => '150'
            ],
            [
                'id' => 2, 
                'name' => 'Black Coffee',
                'caffeine' => '95'
            ],
            [
                'id' => 3, 
                'name' => 'Americano',
                'caffeine' => '77'
            ],
            [
                'id' => 4, 
                'name' => 'Sugar free NOS',
                'caffeine' => '260'
            ],
            [
                'id' => 5, 
                'name' => '5 Hour Energy',
                'caffeine' => '200'
            ],
        ];

        $caffeinatedDrinks = $this->table('caffeinated_drinks');

        $caffeinatedDrinks
            ->insert($data)
            ->saveData();
    }
}
