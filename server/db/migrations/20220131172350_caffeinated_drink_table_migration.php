<?php

use Phinx\Migration\AbstractMigration;

final class CaffeinatedDrinkTableMigration extends AbstractMigration
{
    public function change(): void
    {
        //
        $table = $this
            ->table('caffeinated_drinks');
        
        // Column options
        $table
            ->addColumn('name', 'string', [
                'limit' => 255
            ])
            ->addColumn('caffeine', 'double', [
                'default' => 0,
            ])
            ->addColumn('servings', 'integer', [
                'default' => 1,
            ]);
            
        // Miscellaneous options (timestamps, etc)
        $table
            ->addTimestamps();

        // 
        $table
            ->create();

        if ($this->isMigratingUp) {
            $data = [
                [
                    'id' => 1,
                    'name' => 'Monster Ultra Sunrise',
                    'caffeine' => 75,
                    'servings' => 2
                ],
                [
                    'id' => 2, 
                    'name' => 'Black Coffee',
                    'caffeine' => 95,
                    'servings' => 1
                ],
                [
                    'id' => 3, 
                    'name' => 'Americano',
                    'caffeine' => 77,
                    'servings' => 1
                ],
                [
                    'id' => 4, 
                    'name' => 'Sugar free NOS',
                    'caffeine' => 130,
                    'servings' => 2
                ],
                [
                    'id' => 5, 
                    'name' => '5 Hour Energy',
                    'caffeine' => 200,
                    'servings' => 1
                ],
            ];
    
            $table->insert($data);
            
            $table->saveData();                
        }
    }
}
