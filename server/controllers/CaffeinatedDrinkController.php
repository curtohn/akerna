<?php

namespace Akerna\Controllers;

// Utility imports
use Akerna\Utilities\Cache;
use Akerna\Utilities\APIResponse;

// Model imports
use Akerna\Models\CaffeinatedDrink;

// Third-party module imports
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class CaffeinatedDrinkController 
{
    protected static $cachePrefix = 'caffeinated-drinks';

    public function all(Request $request, Response $response, array $args): Response
    {
        // Set the default "method" of retrieval to "cache" as it's assumed that this will be the most
        // likely method of retrieval (outside of the first in any given 24-hour period)
        $method = 'cache';

        // Because we access the cache multiple times, we want to ensure we're storing the key in a variable
        $cacheKey = self::$cachePrefix . ':all';

        // Check to see if the cache is storing the caffeinated drinks (if it is, we can skip the database)
        $caffeinatedDrinks = Cache::get($cacheKey);
        
        // If the cache is empty, we need to retrieve the caffeinated drinks from the database
        if (!$caffeinatedDrinks) {
            // Set the method to "database" as we're going to have to retrieve the caffeinated drinks from the database
            $method = 'database';

            // Retrieve the caffeinated drinks from the database
            $caffeinatedDrinks = CaffeinatedDrink::all();
            
            // Cache the list to reduce overhead on subsequent calls
            Cache::set($cacheKey, $caffeinatedDrinks);
        }

        return APIResponse::success([
            'success' => true,
            'method' => $method,
            'caffeinated_drinks' => $caffeinatedDrinks,
        ]);
    }

    public function get(Request $request, Response $response, array $args): Response
    {
        // Fetch the ID from the passed in URL parameters (/{id})
        $id = $args['id'];

        // If ID happened to not be set, return an error
        if (!$id) {
            return APIResponse::error([
                'success' => false,
                'error' => 'Missing required parameter: id',
            ]);
        }

        // Because we access the cache multiple times, we want to ensure we're storing the key in a variable
        $cacheKey = self::$cachePrefix . ':' . $id;

        // Set the default "method" of retrieval to "cache" as it's assumed that this will be the most
        // likely method of retrieval (outside of the first in any given 24-hour period)
        $method = 'cache';

        // Check to see if the cache is storing the caffeinated drinks (if it is, we can skip the database)
        $caffeinatedDrink = Cache::get($cacheKey);
        
        // If the cache is empty, we need to retrieve the caffeinated drinks from the database
        if (!$caffeinatedDrink) {
            // Set the method to "database" as we're going to have to retrieve the caffeinated drinks from the database
            $method = 'database';

            // Retrieve the caffeinated drinks from the database
            $caffeinatedDrink = CaffeinatedDrink::find($id);
            
            // If the caffeinated drink was not found, return an error
            if (!$caffeinatedDrink) {
                return APIResponse
                    ::error([
                        'success' => false,
                        'error' => 'Caffeinated drink not found',
                    ]);
            }

            // Cache the list to reduce overhead on subsequent calls
            Cache::set($cacheKey, $caffeinatedDrink);
        }

        return APIResponse::success([
            'success' => true,
            'method' => $method,
            'caffeinated_drink' => $caffeinatedDrink,
        ]);
    }

    public function create(Request $request, Response $response, array $args): Response
    {
        // Get the request body
        $body = $request->getParsedBody();

        $errors = self::validate($body);

        if (!empty($errors)) {
            return APIResponse::error([
                'success' => false,
                'errors' => $errors
            ], 422);
        }

        // Create a new caffeinated drink
        $caffeinatedDrink = CaffeinatedDrink::updateOrCreate([
            'name' => $body['name'] ?? null
        ], [
            'caffeine' => $body['caffeine'] ?? 0
        ]);

        self::refresh();

        return APIResponse::success([
            'success' => true,
            'caffeinated_drink' => $caffeinatedDrink,
        ]);
    }

    public function update(Request $request, Response $response, array $args): Response
    {
        // Fetch the ID from the passed in URL parameters (/{id})
        $id = $args['id'];

        // If ID happened to not be set, return an error
        if (!$id) {
            return APIResponse::error([
                'success' => false,
                'error' => 'Missing required parameter: id',
            ]);
        }
        
        $caffeinatedDrink = CaffeinatedDrink::find($id);

        if (!$caffeinatedDrink) {
            return APIResponse::error([
                'success' => false,
                'error' => 'Caffeinated drink not found',
            ]);
        }

        $body = $request->getParsedBody();

        $errors = self::validate($body);

        if (!empty($errors)) {
            return APIResponse::error([
                'success' => false,
                'errors' => $errors
            ], 422);
        }

        $caffeinatedDrink->name = $body['name'] ?? $caffeinatedDrink->name;
        $caffeinatedDrink->caffeine = $body['caffeine'] ?? $caffeinatedDrink->caffeine;
        $caffeinatedDrink->servings = $body['servings'] ?? $caffeinatedDrink->servings;

        $caffeinatedDrink->save();

        // Remove this caffeinated drink from the cache
        self::refresh($caffeinatedDrink);

        return APIResponse::success([
            'success' => true,
            'caffeinated_drink' => $caffeinatedDrink,
        ]);
    }

    public function delete(Request $request, Response $response, array $args): Response
    {
        // Fetch the ID from the passed in URL parameters (/{id})
        $id = $args['id'];

        // If ID happened to not be set, return an error
        if (!$id) {
            return APIResponse::error([
                'success' => false,
                'error' => 'Missing required parameter: id',
            ]);
        }

        $caffeinatedDrink = CaffeinatedDrink::find($id);

        if (!$caffeinatedDrink) {
            return APIResponse::error([
                'success' => false,
                'caffeinated_drink' => null,
            ]);
        }

        $caffeinatedDrink->delete();

        // Remove this caffeinated drink from the cache
        self::refresh($caffeinatedDrink);

        return APIResponse::success([
            'success' => true,
            'caffeinated_drink' => $caffeinatedDrink,
        ]);
    }

    protected static function validate(array $body)
    {        
        global $validationFactory;

        $uniqueValidation = 'required|min:3|unique:caffeinated_drinks,name';

        if (isset($body['id'])) {
            $uniqueValidation = 'required|min:3|unique:caffeinated_drinks,name,' . $body['id'];
        }

        $rules = [
            'name' => $uniqueValidation,
            'caffeine' => 'required|numeric|min:0|max:500',
            'servings' => 'required|numeric|min:0|max:500',
        ];
        
        $validator = $validationFactory->make($body, $rules, [
            'max' => ':attribute must be less than :max.',
            'min' => ':attribute must be more than :min.',
            'unique' => 'The :attribute field must be unique.',
            'required' => 'The :attribute field is required.',
        ]);
        
        if ($validator->fails()) {
            $messages = [];
            $errors = $validator->errors();

            foreach ($errors->all() as $message) {
                $messages[] = $message;
            }

            return $messages;
        }

        return [];
    }

    protected static function refresh(CaffeinatedDrink $caffeinatedDrink = null): void
    {
        // TODO: Move this into an event queue, so that we can dispatch a job which 
        // can take care of both (perhaps more) caching and database updates
        Cache::del(self::$cachePrefix . ':all');

        if ($caffeinatedDrink) {
            Cache::del(self::$cachePrefix . ':' . $caffeinatedDrink->id);
        }
    }
}