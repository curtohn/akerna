# Akerna - PHP Assessment

## Notes
In completing this assessment, I chose not to use a full-fledged framework such as Laravel, so as to allow myself the opportunity to demonstrate several core competencies, rather than having them be assumed/automated by a framework. 

## Running Locally (Docker)
To run this locally, you MUST have Docker installed (https://www.docker.com/products/docker-desktop).

Once you have installed Docker, please ensure that it is running by doing one of the following:

* Checking your task bar for the Docker icon (it should be static and not initializing)
* Inside a terminal/command prompt, enter `docker` (a list of commands should appear)

Inside a terminal/command prompt, enter the following command: `docker-composer up`. If you do not care to see the visual output of the stack, you can run the same command, modified, with the detatched flag (`docker-compose up -d`).

Within moments, the application stack should spawn, and you should be able to access the client application by navigating to (http://localhost:81). 

If you wish to check out the API directly, you can access that on (http://localhost:82).